package com.extweb4j.core.kit;

import java.text.SimpleDateFormat;
import java.util.Date;
/**
 * 日期格式化
 * @author Administrator
 *
 */
public class DateKit {

	public static final SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	/**
	 * 默认格式化日志
	 * @param date
	 * @return
	 */
	public static String format(Date date){
		
		return SDF.format(date);
		
		
	}
	/**
	 * 格式化日期
	 * @param date
	 * @param pattern
	 * @return
	 */
	public static String format(Date date,String pattern){
		
		return new SimpleDateFormat(pattern).format(date);
		
	}
}	
