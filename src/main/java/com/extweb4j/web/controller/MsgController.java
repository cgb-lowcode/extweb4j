package com.extweb4j.web.controller;

import java.util.HashMap;
import java.util.Map;

import com.extweb4j.core.anno.AuthAnno;
import com.extweb4j.core.controller.ExtController;
import com.extweb4j.core.enums.MsgLevel;
import com.extweb4j.core.enums.MsgStatus;
import com.extweb4j.core.kit.LoginKit;
import com.extweb4j.core.model.ExtMsg;
import com.extweb4j.core.model.ExtUser;
import com.jfinal.plugin.activerecord.Page;
/**
 * 系统消息控制器
 * @author Administrator
 *
 */
public class MsgController extends ExtController{
	/**
	 * 列表
	 */
	@AuthAnno
	public void list(){
		int page = getParaToInt("page");
		int limit = getParaToInt("limit");
		String keywords = getPara("keywords");
		
		Page<ExtMsg> pageData = ExtMsg.dao.paginateByKeywaord(page, limit,keywords);
		renderJson(pageData);
	}
	/**
	 * 获取我的消息
	 */
	public void getmemsg(){
		ExtUser user = LoginKit.getSessionUser(getRequest());
		String uid = user.getId();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("all",ExtMsg.dao.countBy("status = ? and uid = ?", MsgStatus.待处理.getState(),uid));
		map.put("warn",ExtMsg.dao.countBy("status = ? and level = ? and uid = ? ", MsgStatus.待处理.getState(),MsgLevel.警告.getState(),uid));

		renderJson(map);
	}
	/**
	 * 确认处理消息
	 */
	public void confirm(){
		
		String id = getPara("id");
		
		ExtMsg msg = ExtMsg.dao.findById(id);
		msg.set("status", MsgStatus.已完成.getState());
		msg.update();
		success();
	}
}
